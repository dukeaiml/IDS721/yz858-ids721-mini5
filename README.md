# yz858-ids721-mini5

This project extends from mini-project-4 to use a database to calculate total prices of the given fruits. More specifically, instead of getting the price of the fruit from local HashMap, the service retrieves the most up-to-date fruit price from AWS RDS.

## Requirements
- [x] Functionality: create a Rust AWS lambda function / app runner (30%)
- [x] Database integration: connect to a database (30%)
- [x] Service implementation (30%)
- [x] Documentation (10%)

## Steps
1. Create a cloud MySQL database using AWS RDS
RDS Database identifier: miniproject
Database name: mini5
Master username: admin
Master password: 123123123
Endpoint: miniproject.c1qqyssqothg.us-west-2.rds.amazonaws.com
Port: 3306
Inbound security group set to 0.0.0.0/0

2. Insert data into database for later use using CloudShell
```
// connect to db using mysql CLI
// SYNTAX: mysql -h your-db-endpoint -u your-username -p your-database-name
mysql -h miniproject.c1qqyssqothg.us-west-2.rds.amazonaws.com -P 3306 -u admin -p

// Show all the databases, use a specific database, show tables within a database
SHOW DATABASES;
USE [DATABASE_NAME]
SHOW TABLES;


// SQL command to create a new database and table
CREATE DATABASE mini5;
CREATE TABLE fruitprice (
    fruit VARCHAR(255) PRIMARY KEY,
    price FLOAT
);

//Insert data points into the table
INSERT INTO fruitprice (fruit, price)
VALUES 
  ('apple', 1.5),
  ('pear', 1.3),
  ('orange', 2.0);

// Check whether data has been successfully inserted
SELECT * 
FROM fruitprice;
```

3. Connect to and query the database in rust lambda project

4. Build and deploy lambda function
```
cargo lambda build --release
cargo lambda deploy
```

## Screenshots
1. AWS RDS MySQL Database creation
![DbCreation](/screenshots/DatabaseCreated.png)

2. Database creation and data insertion through CloudShell 'mysql' CLI
![DbCreation](/screenshots/Creation.png)
![DbInsertion](/screenshots/Insertion.png)

3. Database creation and data insertion through CloudShell 'mysql' CLI
![Db](/screenshots/Creation.png)
![DbInsert](/screenshots/Insertion.png)

4. Lambda function deployed
![FunctionDeployment](/screenshots/DeployedFunction.png)

5. Lambda function's successful execution
![FunctionExecution](/screenshots/SuccessExecution.png)