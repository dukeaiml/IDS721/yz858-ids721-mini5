use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use mysql::*;
use mysql::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    fruitlist: Vec<String>,
}

#[derive(Serialize)]
struct Response {
    totalprice: f64
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Connect to database
    let url = "mysql://admin:123123123@miniproject.c1qqyssqothg.us-west-2.rds.amazonaws.com:3306/mini5";
    let pool = Pool::new(url)?;
    let mut conn = pool.get_conn()?;

    let fruitlist = event.payload.fruitlist;
    let mut totalprice = 0.0;
    for fruit in fruitlist.iter() {
        // Query fruit price from database
        let sql = format!("SELECT price FROM fruitprice WHERE fruit = '{}'", fruit);
        let price: f64 = conn.query(sql)?.get(0).copied().unwrap_or(0.0);
        totalprice += price;
    }

    let resp = Response { totalprice };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}